FROM ubuntu:18.04
WORKDIR /usr/src/app

RUN apt-get update -y
RUN apt-get install default-jre default-jdk -y --no-install-recommends
RUN apt-get install openjdk-17-jre openjdk-17-jdk -y --no-install-recommends
RUN export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64/

EXPOSE 8080
COPY . /usr/src/app

