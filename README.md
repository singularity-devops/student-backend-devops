# student-backend-devops

Spring REST API application that can create, print, and update students using Postgres as a database of students. All application is deployed using Docker.

## Execution
To run the project just type in the terminal

```bash
docker-compose up -d --build
```

Application is running on port 8080. And has a proxy pass to localhost default port 80.

## Testing
You can test the endpoints with the following commands:
```bash
curl http://localhost:8080/students
```

You could also use POST, PUT, DELETE actions to the above endpoint.

## RabbitMQ
This application also has a connection to RabbitMQ that handles all the messages form the POST `/students` endpoint.

## Structure
This application uses the software pattern used in the picture below.
![structure](https://i.imgur.com/4JGcWYV.png)