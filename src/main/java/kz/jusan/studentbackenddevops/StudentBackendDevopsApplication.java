package kz.jusan.studentbackenddevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentBackendDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentBackendDevopsApplication.class, args);
	}

}
