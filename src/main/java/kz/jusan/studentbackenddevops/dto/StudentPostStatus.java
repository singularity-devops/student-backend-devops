package kz.jusan.studentbackenddevops.dto;

import kz.jusan.studentbackenddevops.student.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StudentPostStatus {
    private Student student;
    private String status;
    private String message;
}
