package kz.jusan.studentbackenddevops.publisher;

import kz.jusan.studentbackenddevops.config.MessagingConfig;
import kz.jusan.studentbackenddevops.dto.StudentPostStatus;
import kz.jusan.studentbackenddevops.student.Student;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
public class StudentPublisher {

    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public String postStudent(@RequestBody Student student) {
        StudentPostStatus studentPostStatus = new StudentPostStatus(
                student, "PROCESS", "student placed successfully");
        template.convertAndSend(MessagingConfig.STUDENT_EXCHANGE, MessagingConfig.STUDENT_ROUTING_KEY, studentPostStatus);
        return "Success!";
    }
}
