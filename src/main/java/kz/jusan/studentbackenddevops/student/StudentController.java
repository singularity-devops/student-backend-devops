package kz.jusan.studentbackenddevops.student;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    @GetMapping
    public List<Student> getAllStudents() {
        return studentService.findAllAccounts();
    }

    @GetMapping("{studentId}")
    public Student getStudentById(@PathVariable("studentId") Long studentId) {
        return studentService.getStudent(studentId);
    }
    /*@PostMapping
    public void registerStudent(@RequestBody Student student) {
        studentService.addNewStudent(student);
    }*/

    @DeleteMapping
    public void deleteAllStudents() {
        studentService.deleteAllStudents();
    }

    @PutMapping("{accountId}")
    public void updateStudent(@PathVariable("accountId") Long studentId,
                              @RequestParam String major ) {
        studentService.updateStudent(studentId, major);
    }

}
