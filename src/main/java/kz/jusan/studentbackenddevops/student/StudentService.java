package kz.jusan.studentbackenddevops.student;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.beans.Transient;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<Student> findAllAccounts() {
        return studentRepository.findAll();
    }
    public void addNewStudent(Student student) {
        studentRepository.save(student);
    }

    public void deleteAllStudents() {
        studentRepository.deleteAll();
    }

    public Student getStudent(Long studentId) {
        return studentRepository.findStudentById(studentId);
    }

    @Transactional
    public void updateStudent(Long studentId, String major) {
        Student student = studentRepository.findStudentById(studentId);
        student.setMajor(major);

    }
}
