package kz.jusan.studentbackenddevops.consumer;

import kz.jusan.studentbackenddevops.config.MessagingConfig;
import kz.jusan.studentbackenddevops.dto.StudentPostStatus;
import kz.jusan.studentbackenddevops.student.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StudentConsumer {

    private final StudentService studentService;

    @RabbitListener(queues = MessagingConfig.STUDENT_QUEUE)
    public void consumeMessageFromQueue(StudentPostStatus studentPostStatus) {
        System.out.println(studentPostStatus);
        studentService.addNewStudent(studentPostStatus.getStudent());
    }
}
